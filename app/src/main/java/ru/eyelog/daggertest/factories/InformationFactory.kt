package ru.eyelog.daggertest.factories

class InformationFactory(
    private val secondPartProducer: SecondPartProducer
    ) {

    private val firstPart = "First"

    fun getInformation(): String{
        return "$firstPart, ${secondPartProducer.getSecondPart()}"
    }
}