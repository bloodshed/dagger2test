package ru.eyelog.daggertest.factories

class ThirdPartProducer {

    private val thirdPart = "Third"

    fun getThirdPart(): String{
        return thirdPart
    }
}