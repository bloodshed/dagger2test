package ru.eyelog.daggertest.factories

class SecondPartProducer(private val thirdPartProducer: ThirdPartProducer) {

    private val secondPart = "Second"

    fun getSecondPart(): String{
        return "$secondPart, ${thirdPartProducer.getThirdPart()}"
    }
}