package ru.eyelog.daggertest.withoutdagger

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import ru.eyelog.daggertest.R
import ru.eyelog.daggertest.factories.InformationFactory
import ru.eyelog.daggertest.factories.SecondPartProducer
import ru.eyelog.daggertest.factories.ThirdPartProducer

class WithoutDaggerActivity : AppCompatActivity() {

    private val thirdPartProducer = ThirdPartProducer()
    private val secondPartProducer = SecondPartProducer(thirdPartProducer)
    private val informationFactory = InformationFactory(secondPartProducer)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvTitle.text = informationFactory.getInformation()
    }
}