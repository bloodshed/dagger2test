package ru.eyelog.daggertest.withdagger

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.eyelog.daggertest.R
import ru.eyelog.daggertest.factories.InformationFactory
import ru.eyelog.daggertest.withdagger.di.DaggerWithDaggerComponent
import javax.inject.Inject

class WithDaggerActivity : AppCompatActivity() {

    @Inject
    lateinit var informationFactory: InformationFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerWithDaggerComponent.create().inject(this)

        tvTitle.text = informationFactory.getInformation()
    }
}