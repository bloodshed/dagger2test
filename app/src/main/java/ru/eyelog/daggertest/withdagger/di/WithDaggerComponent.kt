package ru.eyelog.daggertest.withdagger.di

import dagger.Component
import ru.eyelog.daggertest.withdagger.WithDaggerActivity

@Component(
    modules = [WithDaggerModule::class]
)
interface WithDaggerComponent {

    @Component.Factory
    interface Factory{
        fun create(): WithDaggerComponent
    }

    fun inject(activity: WithDaggerActivity)
}