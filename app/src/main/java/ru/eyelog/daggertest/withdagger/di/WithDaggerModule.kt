package ru.eyelog.daggertest.withdagger.di

import dagger.Module
import dagger.Provides
import ru.eyelog.daggertest.factories.InformationFactory
import ru.eyelog.daggertest.factories.SecondPartProducer
import ru.eyelog.daggertest.factories.ThirdPartProducer

@Module
class WithDaggerModule {

    @Provides
    fun provideThirdPartProducer() = ThirdPartProducer()

    @Provides
    fun provideSecondPartProducer(thirdPartProducer: ThirdPartProducer) = SecondPartProducer(thirdPartProducer)

    @Provides
    fun provideInformationFactory(secondPartProducer: SecondPartProducer) = InformationFactory(secondPartProducer)
}